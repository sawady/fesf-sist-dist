-module(cache).
-export([new/0, add/4, remove/2, lookup/2]).

new() -> [].

add(Domain, Expire, Reply, Cache) -> [ { Domain, Expire, Reply } | Cache ].

lookup(_, []) -> unknown;

lookup(Domain, [ { Domain, Expire, Reply } | _ ]) -> 
    case time:valid(Expire, time:now()) of  
        true -> { ok, Reply };
        false -> invalid
    end;
    
lookup(Domain, [ _ | Cache ]) ->
    lookup(Domain, Cache).
    
remove(_, []) -> [];
remove(Domain, [ { Domain, _, _ } | Cache ]) -> Cache;
remove(Domain, [ D | Cache ]) ->
    [ D | remove(Domain, Cache) ].

