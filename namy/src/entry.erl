-module(entry).
-export([new/0, add/3, lookup/2, remove/2]).

new() -> [].

add(Name, Entry, Entries) -> 
    [ {Name, Entry} | Entries ].

lookup(Req, []) -> unknown;
lookup(Req, [{Req, Entry} | Entries]) -> Entry;
lookup(Req, [ _ | Entries]) -> lookup(Req, Entries).
        
remove(Name, []) -> [];
remove(Name, [Name | Entries]) -> Entries;
remove(Name, [ X | Entries]) -> [ X | remove(Name, Entries)].
