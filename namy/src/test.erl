-module(test).
-export([test_ok/0, test_unknown/0]).

setup() ->
    server:start(s0),
    server:start(s1, com, s0),
    server:start(s2, fesf, s1),
    host:start(host1, www, s2),
    resolver:start(s0).

tear_down() ->
    server:stop(s0),
    server:stop(s1),
    server:stop(s2),
    host:stop(hos1),
    resolver:stop().

test_ok() ->
    setup(),
    client:test([www, fesf, com], resolver),
    tear_down().
    
test_unknown() ->
    setup(),
    client:test([mail, fesf, com], resolver),
    tear_down().
    
test_ttl() ->
    setup(),
    s0 ! { ttl, 1000 },
    s1 ! { ttl, 1000 },
    s2 ! { ttl, 1000 },
    client:test([www, fesf, com], resolver),
    host:stop(host1),
    s1 ! {deregister, fesf},
    s2 ! {deregister,  www},
    host:start(host1, www, s2),    
    tear_down().