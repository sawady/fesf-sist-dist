-module(server).
-export([start/1, start/3, stop/2, init/0, init/2]).

start(ServerName) ->
    register(ServerName, spawn(server, init, [])).

start(ServerName, Domain, DNS) ->
    register(ServerName, spawn(server, init, [Domain, DNS])).

stop(ServerName, Parent) ->
    % nos parecio necesario que cuando un nodo hijo se detiene se
    % desregistre del padre
    Parent ! {deregister, ServerName},
    ServerName ! stop,
    unregister(ServerName).

init() ->
    server(entry:new(), 0).

init(Domain, Parent) ->
    Parent ! {register, Domain, {dns, self()}},
    server(entry:new(), 0).
    
server(Entries, TTL) ->
    receive
        {request, From, Req}->
            io:format("request ~w ", [Req]),
            Reply = entry:lookup(Req, Entries),
            From ! {reply, Reply, TTL},
            server(Entries, TTL);
        {register, Name, Entry} ->
            Updated = entry:add(Name, Entry, Entries),
            server(Updated, TTL);
        {deregister, Name} ->
            Updated = entry:remove(Name, Entries),
            server(Updated, TTL);
        {ttl, Sec} ->
            server(Entries, Sec);
        status ->
            io:format("cache ~w~n", [Entries]),
            server(Entries, TTL);
        stop ->
            io:format("closing down~n", []),
            ok;
        Error ->
            io:format("strange message ~w~n", [Error]),
            server(Entries, TTL)
    end.