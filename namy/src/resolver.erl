-module(resolver).
-export([start/1, stop/0, init/1]).

start(Root) ->
    register(resolver, spawn(resolver, init, [Root])).
    
stop() ->
    resolver ! stop,
    unregister(resolver).
    
init(Root) ->
    Empty = cache:new(),
    Inf = time:inf(),
    Cache = cache:add([], Inf, {dns, Root}, Empty),
    resolver(Cache).

resolver(Cache) ->
    receive
        {request, From, Req}->
            io:format("request ~w ~w~n", [From,Req]),
            {Reply, Updated} = resolve(Req, Cache),
            From ! {reply, Reply},
            resolver(Updated);
        status ->
            io:format("cache ~w~n", [Cache]),
            resolver(Cache);
        stop ->
            io:format("closing down~n", []),
            ok;
        Error ->
            io:format("strange message ~w~n", [Error]),
            resolver(Cache)
    end.
    % hay que agregar un after en caso de servidor de DNS caido

resolve(Name, Cache)->
    io:format("resolve ~w ", [Name]),
    case cache:lookup(Name, Cache) of
        unknown ->
            io:format("unknown ~n ", []),
            recursive(Name, Cache);
        invalid ->
            io:format("invalid ~n ", []),
            recursive(Name, cache:remove(Name, Cache));
        {ok, Reply} ->
            io:format("found ~w ~n ", [Reply]),
            {Reply, Cache}
    end.
    
% resolve ["www", "kth", "se"]
% recursive [ "www" | [ "kth", "se"] ]
% resolve [ "kth", "se" ]
% recursive [ "kth" | ["se"] ]
% resolve [ "se" ]
% recursive [ "se" | [] ]
% resolve []  ---  cache:add([], Inf, {dns, Root}, Empty)

% {ok, Root} -> {Root, Cache}

% recursive [ "se" | [] ]
% Root ! "se"
% se van resolviendo todos DNS
% ...
% recursive [ "www" | [ "kth", "se"] ]
% Srv ! {request, self(), "www" }
% este responde unknown o { host, Reply }

recursive([Name|Domain], Cache) ->
    io:format("recursive ~w ", [Domain]),
    case resolve(Domain, Cache) of
        {unknown, Updated} ->
            io:format("unknown ~n", []),
            {unknown, Updated};
        {{dns, Srv}, Updated} ->
            Srv ! {request, self(), Name},
            io:format("sent ~w request to ~w ~n", [Name, Srv]),
            receive
                {reply, Reply, TTL} ->
                    Expire = time:add(time:now(), TTL),
                    {Reply, cache:add([Name|Domain], Expire, Reply, Updated)}
            end
    end.
