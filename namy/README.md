# Namy

## Introducción

El trabajo de esta semana consistió en la implementación de un servidor de DNS.

## Completitud del código

Fuimos complentando distintas partes del código a medida que se requería. Modificamos
algunas otras para volverlas más claras, para nuestro entendimiento.

Nuestro módulo de cache nos quedó de la siguiente manera:

```
-module(cache).
-export([new/0, add/4, remove/2, lookup/2]).

new() -> [].

add(Domain, Expire, Reply, Cache) -> [ { Domain, Expire, Reply } | Cache ].

lookup(_, []) -> unknown;

lookup(Domain, [ { Domain, Expire, Reply } | _ ]) -> 
    case time:valid(Expire, time:now()) of  
        true -> { ok, Reply };
        false -> invalid
    end;
    
lookup(Domain, [ _ | Cache ]) ->
    lookup(Domain, Cache).
    
remove(_, []) -> [];
remove(Domain, [ { Domain, _, _ } | Cache ]) -> Cache;
remove(Domain, [ D | Cache ]) ->
    [ D | remove(Domain, Cache) ].
```

Entendemos que el orden de complejidad de peor caso de lookup y remove es O(n),
y podría mejorarse usando una tabla de hash o un árbol de búsqueda binaria.

Nos dimos cuenta que cuando un servidor de DNS se detenía, no se deregistraba
de su padre. Por comodidad hicimos esto, sobre todo para la prueba que requería
cambiar el nombre de dominio de un host.

```
stop(ServerName, Parent) ->
    % nos parecio necesario que cuando un nodo hijo se detiene se
    % desregistre del padre
    Parent ! {deregister, ServerName},
    ServerName ! stop,
    unregister(ServerName).
```

Hicimos un módulo llamado `test`, para testear internamente, en una misma máquina,
toda la funcionalidad que implementamos. Ahí exportamos dos funciones principales,
una que simplemente hace un request que se espera exitoso, y otro que hace un
request donde expira un nombre de dominio, que utilizamos para pruebas que describimos
en la última sección de este texto.

Por último, para que ese módulo de test pueda funcionar correctamente, hicimos
que los servidores de DNS tomen su nombre al momento de construcción, así
podían convivir varios en una misma máquina.
 
## Pruebas

Hicimos una prueba similar a la del PDF. Iniciamos varios DNS, con un host
y un cliente. Como no teníamos suficientes PCs lo que hicimos fue levantar varias
consolas en nuestras máquinas, pero intentando que haya saltos por la red para
conseguir las diferentes cosas. Estos fueros los pasos que realizamos:

`rebar3 shell --name 'root@IP_ELIAS' --setcookie dns`
`server:start()`

`rebar3 shell --name 'com@IP_FEDE' --setcookie dns`
`server:start(com, {server, 'root@IP_ELIAS'})`

`rebar3 shell --name 'fesf@IP_ELIAS' --setcookie dns`
`server:start(fesf, {server, 'com@IP_FEDE'})`

`rebar3 shell --name 'www@IP_FEDE' --setcookie dns`
`host:start(www, www, {server, 'fesf@IP_ELIAS'})`
`host:start(mail, mail, {server, 'fesf@IP_ELIAS'})`
`resolver:start({server, 'root@IP_ELIAS'})`
`client:test([www, fesf, com], resolver)`

Los resultados fueron variados, dependiendo de cómo estaba la red en ese momento.
Vale aclarar que usamos una red privada con Hamachi, y eso hace que la latencia
sea mayor de la que debería si nos pudieramos comunicar entre nuestras PCs
de forma directa.

## Análisis y Conclusiones

### Modificación del TTL

"En la configuración simple el ttl está puesto en 0.
¿Qué sucede si cambiamos esto a 2 o 4 segundos?"
"¿Cuánto se reduce el tráfico?"

Con el TTL en 0, el dato de la cache siempre va a ser inválido, por lo tanto 
siempre va a tener que recurrir a la consulta a los servidores de DNS. 
Si aumentamos el TTL,
más tiempo tardará en ser invalidad en la cache, pero es posible que el, si el
host que estamos consultando cambia de nombre, obtengamos respuestas erróneas.

"Cambiar el TTL a un par de minutos y mover los hosts, esto es apagarlos e iniciarlos 
registrándolos bajo un nuevo nombre."

"¿Cuándo se encuentra el nuevo server?"

Luego de vencido el TTL.

"¿cuantos nodos necesitan saber sobre el cambio?"

Tantos como subdominios se hayan modificado de la URI del host, 
y no esten ya registrados en sus respectivos servers de DNS. 

### Organización de la Cache

"Nuestra cache tambien tiene el problema de que hay entries que nunca son
eliminadas. Las entries inválidas se eliminan y se actualizan pero si nunca se
busca una entry nunca se eliminará. ¿Cómo puede la cache organizarse mejor?"

Una opción es que algún proceso haga pasada 
cada tanto tiempo, y elimine entries inválidas.

"¿Cómo podemos reducir el tiempo de búsqueda? ¿Podemos usar una tabla de hash o un árbol?"

Es posible usar un árbol o una tabla de hash para agilizar el tiempo de búsqueda
en la cache. Nosotros estamos usando una lista, y el uso de una tabla de hash
o un árbol de búsqueda binaria reduciría la complejidad en peor caso del algoritmo
a O(1) u O(log n). Si se hace una tabla de hash lo suficientemente grande para
evitar colisiones, funcinaría muy bien.
