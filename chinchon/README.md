# Trabajo Final: Chinchón
## Materia: Sistemas Distribuidos
## Integrantes: Federico Sawady y Elías Filipponi

# Introducción

El chinchón es un juego de mesa multijugador. 
Sus reglas fueron simplificadas para los fines del presente TP. 

## Reglamento

Para ganar el chinchón, uno debe acumular la menor cantidad de puntos posible. 
Dichos puntos surgen de las cartas que no pudo combinar con otras al terminar la ronda.

## Inicio

Al iniciar la partida, se mezclan y reparten las cartas, 7 a cada jugador, 
el resto del mazo queda boca abajo en la mesa. Quien reparte da vuelta una primer 
carta del mazo ubicándola al lado del mismo, y se da comienzo a la primer ronda.

## Ronda

Cada jugador debe tratar de formar combinaciones que le permitan involucrar la 
mayor cantidad de cartas. Durante su turno tiene opción de cambiar solo una de 
sus cartas por otra de la mesa. Puede optar por la carta visible o dar vuelta 
una del mazo. Si ésta última no le sirve, tiene opción de dejarla sobre la 
visible, junto al mazo. La ronda estará completa cuando alguno de los jugadores 
la cierre.

## Cierre de ronda o corte

Un jugador puede cerrar la ronda al final de su turno si se cumple alguna de las 
siguientes condiciones:
- tiene una combinación válida (simple o mixta) de 7 cartas 
(de ser simple, el jugador tiene chinchón y resta 10 puntos de su cuenta)
- tiene una combinación válida (simple o mixta) de 6 cartas pero la restante 
suma hasta 3 puntos (en cuyo caso el jugador acumula esos puntos restantes).

## Acomodamiento

Una vez cerrada la ronda formal los jugadores muestran sus cartas. 
Esta es la última oportunidad de deshacerse de cartas no combinadas que 
tendrá el jugador, pero debe actuar rápido. 

Si alguna de sus cartas no combinadas sirve para completar o agrandar 
(a un tamaño válido) una combinación presentada por otro jugador, podrá 
ubicar esa carta restante en el juego de ese jugador, librándose así de los 
puntos de esa carta. 

De haber más de un interesado en aprovechar las cartas de un jugador, la 
prioridad se asigna por orden de llegada, y siempre que la acción propuesta 
resulte en una combinación (simple o mixta) válida para el jugador receptor 
de la carta (no puede, por ejemplo, terminar con una combinación mixta de 4 y 
4 ni una escalera de 8 cartas).

Toda carta no combinada suma puntos para el jugador.

## Juegos o combinaciones posibles

Simples: un grupo compuesto de 6 o 7 cartas consecutivas del mismo palo 
(si son 7, se posee chinchón).
Mixtas: combinación de grupos (“3 y 3” o “4 y 3”) formados cada uno según 
alguna de las siguientes formas:
 - Escalera (cartas con números consecutivas del mismo palo)
 - Pierna (cartas de igual número
 
# Diseño de la solución

La partida estará distribuida y tendrá jugadores humanos. 
Cada jugador tendrá la partida y el juego será tolerante a fallos así como a 
la desconexión de jugadores.

## Tipos Abstractos

```
Card
suit: String
number: Int

Player
name: String
cards: List<Card>
combinations: List<List<Card>>

Game
cardDeck: List<Card>
currentPlayers: List<(PlayerName, Int)>
player: Player
multicaster: Multicaster

Multicaster
peers: List<PID>
```

## Arquitecura del sistema distribuido

Cada jugador es un proceso, en forma de juego y datos del jugador. 
Cada juego conoce a un proceso de multicast, 
y a su vez este conoce a sus pares multicast. Los multicasters serán los responsables
de comunicar mensajes y sincronizarse de distintas formas, y los juegos serán
los responables de la lógica de las distintas etapas del juego.

Para empezar una partida, cada nodo que quiere jugar, arranca un proceso de multicast, 
informando luego quienes son sus pares. Una vez que posee esa información, se crea 
un Game en cada nodo y se le informa al multicast su proceso master, 
que es este Game.

Cada Game se presenta a los demás mandando su nombre. Los multicast deciden en qué 
orden entregar esos nombres, dando así el orden de cada Game dentro del juego. 
Cada Game entonces, agrega a su lista de otros jugadores, el nombre de cada jugador.

El que tiene el primer turno mezcla y reparte las cartas, y comunicando 
el mazo y cartas a todos.

El proceso que se encuentre como siguiente en la lista de jugadores que posee
cada uno es el que arranca la ronda. Juega su turno y comunica que termina,
comunicando su jugada al resto.

Cuando un jugador tiene lo necesario para cortar, puede hacerlo, comunicando
al resto que ha cortado, al igual que sus cartas. Ahí inicia el proceso de 
acomodamiento. Cada jugador indica al jugador que cortó que cartas quiere acomodar,
todos al mismo tiempo, y se llegará a un acuerdo sobre el orden en que
se acomodan dichas cartas. De haber una inconsistencia se informa el jugador
que no puede acomodar que sus cartas serán devueltas, y al jugador que pudo
acomodar que sus cartas han sido colocadas correctamente. Luego de este
proceso cada jugador contará sus puntos y los compartirán con el resto.

Si se detecta que un jugador alcanzó los 101 puntos, se dá por finalizada la partida.
Cada proceso realiza un saludo final para dar por terminado el encuentro.

# Etapas de implementación

Habrá un conjunto de tests de integración, y unitarios para algunas entidades.

El proyecto se llevará a cabo a través de los siguientes pasos:

1) Crear el proceso de Multicast, Game y Jugador, y llegar a que 
se presenten entre todos, quedando sincronizados a partir de ese momento.

2) Hacer que los jugadores se sincronicen para elegir a uno para repartir (random).
Implementar un mecanismo de comunicación del mazo, para que los demás lo tengan
luego de repartir.

3) Implementar la lógica de una jugada y pasar al siguiente turno, y la lógica 
para terminar una ronda, incluyendo los puntos que cada jugador se suma
en dicha ronda.

5) Terminar un juego completo, sin acomodamiento de cartas.

6) Agregar la lógica para la etapa de acompodamiento.