-module(player).
-export([create/2, name/1, play/2, test/0]).

create(Name, InitialCards) ->
    { Name, InitialCards, [] }.

name({ Name, _, _ }) ->
	Name.

play(Card, { Name, Cards, Combos }) ->
	timer:sleep(1000),
	io:format("Cartas sin juego: ~p~n", [Cards]),
	io:format("Juegos terminados: ~p~n", [Combos]),
	io:format("Carta en mesa: ~p~n", [Card]),
	case io:read("Selecciona\n" ++
		"1. cortar\n" ++
		"2. armar juego\n" ++
		"3. desarmar juego\n" ++
		"4. intercambiar con visible\n" ++
		"5. pasar turno\n" ++
		"Opcion ingresada: ") of
		{ ok, 1 } -> 
			{ finish, Combos, { Name, Cards, Combos } };
		{ ok, 2 } -> 
			{ NewCard, NewCards, NewCombos } = groupCards(Card, Cards, Combos),
			play(NewCard, { Name, NewCards, NewCombos });
		{ ok, 3 } -> 
			{ NewCard, NewCards, NewCombos } = ungroupCards(Card, Cards, Combos),
			play(NewCard, { Name, NewCards, NewCombos });
		{ ok, 4 } -> 
			{ NewCard, NewCards } = changeVisible(Card, Cards),
			play(NewCard, { Name, NewCards, Combos });
		{ ok, 5 } -> 
			{ pass, Card, { Name, Cards, Combos } };
		_ -> 
			io:format("Opcion Invalida!!!~n~n"),
			play(Card, { Name, Cards, Combos })
	end.

changeVisible(Card, Cards) ->
	case io:read("Elija carta a intercambiar: ") of
		{ ok, N } ->
			C = lists:nth(N, Cards),
			{ C, [ Card | lists:delete(C, Cards) ] };
		_ ->
			io:format("Opcion Invalida!!!~n~n"),
			changeVisible(Card, Cards)
	end.

groupCards(Card, Cards, Combos) ->
	case io:read("Elija cartas para armar juego: ") of
		{ ok, XS } ->
			{ Combo, NewCards } = lists:foldr(
				fun(X, { CS, NC }) -> 
					C = lists:nth(X, NC),
					{ [ C | CS ], lists:delete(C, NC) }
				end, { [], Cards }, XS),
				{ Card, NewCards, [ Combo | Combos ] };		
		_ ->
			io:format("Opcion Invalida!!!~n~n"),
			groupCards(Card, Cards, Combos)
	end.

ungroupCards(Card, Cards, Combos) ->
	case io:read("Elija nro de juego a desarmar: ") of
		{ ok, N } ->
			Combo = lists:nth(N, Combos),
			{ Card, Cards ++ Combo, lists:delete(Combo, Combos) };
		_ ->
			io:format("Opcion Invalida!!!~n~n"),
			ungroupCards(Card, Cards, Combos)
	end.

test() ->
	Cards = [
		{7,copa},
        {2,oro},
        {2,copa},
        {2,espada},
        {6,espada},
        {5,oro},
        {3,oro}
    ],
	Player = create("Player 1", Cards),
	play({4, oro}, Player).

finish(Combos) ->
	case length(Combos) > 3 of
		true  -> Combos;
		false -> Combos
	end.

