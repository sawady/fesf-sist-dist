-module(game).
-export([start/3]).

-define(deadlock, 4000).

start(Name, Multicaster, Seed) ->
    spawn(fun() -> init(Name, Multicaster, Seed) end).
    
init(Name, Multicaster, Seed) -> 
	Multicaster ! { master, self() },
	N = waitNumberOfPlayers(Name),
	error_logger:info_msg("~p: game started!~n", [ Name ]),
	Multicaster ! { send, { playerName, Name } },
	waitOtherPlayers(Name, Multicaster, N, [], Seed).

waitNumberOfPlayers(Name) -> 
	receive
		stop -> stop;
		{ players, N } -> gameInfo(Name, N, "the number of players"), N
	after ?deadlock ->
		error_logger:info_msg("~p: waiting for number of players!~n", [ Name ]),
		waitNumberOfPlayers(Name)
	end.

waitOtherPlayers(Name, Multicaster, 0, Players, Seed) ->
	gameInfo(Name, Players, "the players are"),
	goToShuffle(Name, Multicaster, Players, Seed);

waitOtherPlayers(Name, Multicaster, N, Players, Seed) ->
	receive
		stop -> stop;
		{ response, { playerName, PlayerName } } -> 
			gameInfo(Name, PlayerName, "player name received"), waitOtherPlayers(Name, Multicaster, N-1, [ PlayerName | Players ], Seed)
	after ?deadlock ->
		gameInfo(Name, "waiting for other players!"),
		waitOtherPlayers(Name, Multicaster, N, Players, Seed)
	end.

goToShuffle(Name, Multicaster, [ Name | Players ], Seed) ->
	gameInfo(Name, "I will shuffle"),
	ShuffledCards = cardDeck:create(Seed, [ Name | Players ]),
	Multicaster ! { send, { cardDeck, ShuffledCards } },
	receiveCardDeck(Name, Multicaster, [ Name | Players ], 0);

goToShuffle(Name, Multicaster, [ OtherName | Players ], _) ->
	gameInfo(Name, OtherName, "will shuffle"),
	receiveCardDeck(Name, Multicaster, [ OtherName | Players ], 0).

receiveCardDeck(Name, Multicaster, PlayerNames, Order) ->
	receive
		stop -> stop;
		{ response, { cardDeck, ShuffledCards } } -> 
			% gameInfo(Name, ShuffledCards, "I receive ShuffledCards"),
			{ PlayersCards, CardDeck } = ShuffledCards,
			{ _, PlayerCards } = lists:keyfind(Name, 1, PlayersCards),
			Player = player:create(Name, PlayerCards),
			gameInfo(Name, PlayerCards, "My cards are"),
			gameInfo(Name, CardDeck, "CardDeck is"),
			game(Name, Multicaster, CardDeck, Player, PlayerNames, Order)
	after ?deadlock ->
		gameInfo(Name, "waiting for Card Deck!"),
		receiveCardDeck(Name, Multicaster, PlayerNames, Order)
	end.

game(Name, Multicaster, CardDeck, Player, PlayerNames, Order) ->
	NewOrder = nextTurn(PlayerNames, Order),
	gameInfo(Name, NewOrder, "next turn"),
	case myTurn(Name, PlayerNames, NewOrder) of
	 	true  -> 
	 		gameInfo(Name, "My Turn"),
	 		NewPlayer = makeMyPlay(Multicaster, CardDeck, Player),
	 		waitPlay(Name, Multicaster, CardDeck, NewPlayer, PlayerNames, NewOrder);
	 	false -> 
	 		gameInfo(Name, "Other Player Turn"),
	 		waitPlay(Name, Multicaster, CardDeck, Player, PlayerNames, NewOrder)
	end.
	
makeMyPlay(Multicaster, { _, CardDeck }, Player) ->
	{ Card, NewCardDeck } = cardDeck:next(CardDeck),
	case player:play(Card, Player) of
		{ pass, RestCard, NewPlayer } ->
			Multicaster ! { send, { cardDeck, { RestCard, NewCardDeck } } },
			NewPlayer;
		{ finish, Combos, NewPlayer } ->
			Multicaster ! { send, { finish, Combos, player:name(NewPlayer) } },
			NewPlayer
	end.
	
waitPlay(Name, Multicaster, CardDeck, Player, PlayerNames, Order) ->
	receive
		stop -> stop;
		{ response, { cardDeck, NewCardDeck } } -> 
			gameInfo(Name, Order, "I receive new cardDeck"),
			game(Name, Multicaster, NewCardDeck, Player, PlayerNames, Order);
		{ response, { finish, Combos, PlayerName } } ->
			gameInfo(Name, Combos, "game finish: "),
			gameInfo(Name, PlayerName, "wins"),
			gameInfo(Name, PlayerName, "Flawless Victory!"),
			Multicaster ! stop,
			self() ! stop
	after ?deadlock ->
		% gameInfo(Name, "waiting for new Card Deck!"),
		waitPlay(Name, Multicaster, CardDeck, Player, PlayerNames, Order)
	end.

nextTurn(PlayerNames, Order) ->
	(Order + 1) rem length(PlayerNames).

myTurn(Name, PlayerNames, Order) ->
	lists:nth(Order+1, PlayerNames) =:= Name.

gameInfo(Name, Info) ->
	error_logger:info_msg("~p: " ++ Info ++ "~n", [ Name ]).

gameInfo(Name, Msg, Info) ->
	error_logger:info_msg("~p: " ++ Info ++ " ~p~n", [ Name, Msg ]).