-module(test).
-export([run/0, run/2]).

run() ->
	run(4, 1000).

run(Size, Jitter) ->
	% generate data
	Data = lists:map(fun(N) ->
		Multicaster  = multicaster:start("MultiCaster " ++ integer_to_list(N), Jitter),
		Name  = "Player " ++ integer_to_list(N),
		Seed  = N,	
		{ Name, Multicaster, Seed }
	end, lists:seq(1, Size)),

	% get all locks
	Multicasters = lists:map(fun({ _, Multicaster, _ }) -> 
		Multicaster
	end, Data),

	% open locks
	lists:foreach(fun(Multicaster) -> 
		Multicaster ! { peers, lists:delete(Multicaster, Multicasters) }
	end, Multicasters),

	% wake up games
	lists:foreach(fun({ Name, Multicaster, Seed }) -> 
		game:start(Name, Multicaster, Seed)
	end, Data).
