-module(cardDeck).
-export([create/2, next/1]).

create(Seed, PlayerNames) -> 
	% random:seed(Seed),
	Cards = shuffle([ card:create(N, S) || 
	   N <- [1,2,3,4,5,6,7,8,9,10,11,12], 
	   S <- [espada, basto, oro, copa] 
	]),
	{ PlayersCards, CardDeck } = dealCards(PlayerNames, Cards),
	{ PlayersCards, next(CardDeck) }.

dealCards(PlayerNames, Cards) -> 
	lists:foldl(fun(P, { PCDR, CDR }) ->
		{ PCS, CDN } = dealToPlayer(P, CDR),
		{ [ PCS | PCDR ], CDN }
	end, { [], Cards }, PlayerNames).

dealToPlayer(P, CD) ->
	{ PCS, CDN } = lists:foldl(fun(_, { PCSR, CDR }) ->
		{ C, CDN } = next(CDR),
		{ [ C | PCSR ], CDN }
	end, { [], CD }, lists:seq(1, 7)),
	{ { P, PCS }, CDN }.

next([]) -> error_logger:error_msg("Next on empty cardDeck"), empty;
next([ C | CD ]) -> { C, CD }.

shuffle([])     -> [];
shuffle([Elem]) -> [Elem];
shuffle(List)   -> shuffle(List, length(List), []).

shuffle([], 0, Result) ->
    Result;
shuffle(List, Len, Result) ->
    {Elem, Rest} = nth_rest(random:uniform(Len), List),
    shuffle(Rest, Len - 1, [Elem|Result]).

nth_rest(N, List) -> nth_rest(N, List, []).

nth_rest(1, [E|List], Prefix) -> {E, Prefix ++ List};
nth_rest(N, [E|List], Prefix) -> nth_rest(N - 1, List, [E|Prefix]).