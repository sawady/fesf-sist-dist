-module(card).
-export([create/2, number/1, suit/1]).

create(N, S) ->
    { N, S }.
    
number({ N, _ }) -> N.

suit({ _, S }) -> S.