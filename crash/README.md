# Crash + Ping Pong

Esto es la prueba de ejercicios de TP1 + ejercicio de ping-pong.

Código principal:
- pingpong.erl

Otras pruebas:
- hello.erl
- wait.erl
- tic.erl

#### Para probar dentro de un mismo nodo hicimos este test:
    $> pingpong:test().

#### Para probar en dos nodos distintos hicimos lo siguiente:
  - PC1 (ping):

    erl -name pcping@IP_1 -setcookie pingpong

    $> pingpong:register_ping().

  - PC2 (pong):

    erl -name pcpong@IP_2 -setcookie pingpong

    $> pingpong:ping_to_node('pcping@IP_1').