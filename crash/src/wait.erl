-module(wait).
-export([hello/1]).

hello(PAUSADO) ->
  if 
    PAUSADO ->
      receive
        resume -> hello(false)
      end;
    true    -> 
      receive
        pause -> hello(true);
        X ->
          io:format("aaa! surprise, a message: ~s~n", [X]),
          hello(false)
      end
  end.
