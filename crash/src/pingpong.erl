-module(pingpong).
-export([test/0, ping/1, pong/1, register_ping/0, ping_to_node/1]).

ping(N) ->
  timer:sleep(100),
  receive
    {ping, P} ->
      io:format("ping ~p: ~w~n", [N, P]),
      P ! {pong, self()},
      ping(N+1)
  end.

pong(N) ->
  timer:sleep(100),
  receive
    {pong, P} ->
      io:format("pong ~p: ~w~n", [N, P]),
      P ! {ping, self()},
      pong(N+1)
  end.

register_ping() ->
  register(pingP, spawn(pingpong, ping, [0])).

ping_to_node(NODE) ->
  PONG = spawn(pingpong, pong, [0]),
  { pingP, NODE } ! {ping, PONG}.

test() ->
  PING = spawn(pingpong, ping, [0]),
  PONG = spawn(pingpong, pong, [0]),
  PING ! {ping, PONG},
  start.






