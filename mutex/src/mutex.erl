-module(mutex).
-export([create/0, lock/1, unlock/1, test/0]).

create() ->
    spawn(fun() -> unlocked() end).

lock(Mutex) ->
    Mutex ! {lock, self()},
    receive
        ok -> ok
    end.

unlock(Mutex) ->
    Mutex ! {unlock, self()}.

unlocked() ->
    receive {lock, LockedBy} ->
        LockedBy ! ok,
        locked(LockedBy, 1)
    end.

locked(LockedBy, Count) ->
    receive
        {unlock, LockedBy} ->
            if 
                Count == 1 -> unlocked();
                true -> locked(LockedBy, Count - 1)
            end;
        {lock, LockedBy} ->
            LockedBy ! ok,
            locked(LockedBy, Count + 1)
    end.

test() ->
    M = mutex:create(),
    mutex:lock(M),
    spawn(fun() -> mutex:lock(M),
        io:format("unlocked!~n", []),
        mutex:unlock(M) end),
    mutex:unlock(M).

