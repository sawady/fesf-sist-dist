# Loggy

## Introducción

La consigna de esta actividad es la de construir un logger que garantice 
el orden en que se imprimen los mensajes enviados por un conjunto de procesos.

Así por ejemplo, si un proceso A manda un mensaje al logger, y luego manda
un mensaje a un proceso B, el cual reacciona y manda su propio mensaje al logger,
querríamos que primero se imprima el mensaje de A y luego el de B.

La cola de mensajes de Erlang no alcanza para resolver esta situación, por lo que
se propone una sincronización a traves de relojes lógicos de Lamport.

Este tipo de sincronización en sistemas distribuidos nos permitirá entonces en garantizar 
que la acción de logging se ejecute en forma cronológica, 
respetando el orden de los eventos dentro del sistema.

## Sincronización propuesta por Lamport

Según Lamport, la sincronización de relojes no tiene que ser sobre tiempos absolutos,
dado que dos procesos que no interactúan no necesitan que sus relojes estén sincronizados.
Lo que sí nos importa es que los procesos estén de acuerdo en la hora, 
para coincidir en el orden en que ocurren los eventos.

Entonces, una solución consiste en implementar un *reloj lógico*. Esto es una
estructura abstracta de datos que propone Lamport, que no es más que un contador 
con una operación de incremento, y que no necesita tener relación con un reloj físico.

Un evento ocurre antes que otro cuando el tiempo en el reloj lógico es menor.

## Primera implementación (mensajes fuera de orden) y primeras preguntas

Primero hicimos una implementación mínima para que anduvieran los tests, como
proponía el enunciado del ejercicio.

### Implementación naive con Tiempo de Lamport

Para la estructura abstracta de datos del reloj de Lamport elegimos elegimos un
número. Incrementarlo será sumarle una unidad, y el resto de las operaciones son
triviales.

```
zero() ->
    0.

inc(Name, T) ->
    T+1.

merge(Ti, Tj) ->
    max(Ti, Tj).

leq(Ti, Tj) ->
    Ti <= Tj.
```

Con esto podemos identificar el orden de impresión que deberían tener los mensajes. Modificamos
los workers para que aumenten su reloj luego de mandar un mensaje a otro worker, y al recibir un mensaje
de otro hacen un `time:merge` actualizando su reloj al máximo entre ambos tiempos.

1) ¿Cómo sabemos que fueron impresos fuera de orden? 
¿Cómo identificamos mensajes que estén en orden incorrecto? 
¿Qué es siempre verdadero y qué es a veces verdadero?

Se imprimió un mensaje de "recibido" antes de imprimirse un mensaje de
"enviando". Miramos el nro de mensaje y vemos esa situación, para chequear
incoherencias. Sabemos que los relojes nos van a ayudar, dado que el tiempo
del reloj lógico del worker que envía el mensaje es menor que el del worker
que lo recibe, y eso nos va a permitir ordenar los mensajes y chequear si son
seguros de imprimir.

2) ¿Cómo sabemos si los mensajes son seguros de imprimir?

Si para un mensaje llegó el "recibido", luego esperamos que se haya producido el
"enviado". Si se recibe primero el "enviado", deberíamos esperar a recibir
el mensaje de la acción que lo produce. Entonces, si dado un tiempo y una lista
de mensajes, podremos imprimir todos aquellos mensajes, ordenados por tiempo,
que sean menores o iguales a ese tiempo dado.

## Implementación con cola de retención

Las otras funciones del módulo `time` nos quedaron de esta manera:

```
% retorna un reloj que pueda llevar cuenta de los nodos
clock(Nodes) ->
    lists:map(fun(N) -> { N, zero() } end, Nodes).

% retorna un reloj que haya sido actualizado
% dado que hemos recibido un mensaje de log de un nodo en determinado
% momento.
update(Node, Time, Clock) ->
	case lists:keymember(Node, 1, Clock) of
		true  -> lists:keyreplace(Node, 1, Clock, { Node, Time });
		false -> [ { Time, Node } | Clock ]
	end.

% retorna true o false si es seguro enviar el mensaje
% de log de un evento que ocurrió en el tiempo Time dado.
safe(Time, Clock) -> 
    lists:all(fun({ N, T }) -> leq(Time, T) end, Clock).
```

Varias funciones sobre listas nos ayudaron a que la implementación sea trivial.
Entonces, como representación elegimos una lista de tuplas nodo-tiempo. Incluso
estando dentro del módulo seguimos usando la interfaz de las otras funciones
sobre `time`.

En la creación de un clock generamos una lista de nodos con tiempos en cero.
Luego cuando llega un mensaje la idea es usar la función `update` para actualizar
el tiempo para ese nodo, y así poder chequear si ese mensaje es seguro o no
de imprimirse.

Luego también está la función que dado un tiempo y un reloj lógico nos dice
si un mensaje para ese tiempo es seguro o no de imprimir, y será seguro
siempre y cuando todos los nodos dentro de la lista tenga un tiempo
mayor al dado por parámetro.

De esta forma modificamos el loop del logger para hacer uso de esta interfaz:

```
loop(Clock, Queue) ->
    receive
        {log, From, Time, Msg} ->
            NewClock = time:update(From, Time, Clock),
            NewQueue = 
                case time:safe(Time, NewClock) of
                    true  -> 
                        log(From, Time, Msg),
                        printSafeMsgs(Time, Queue),
                        notSafeMsgs(Time, Queue);
                    false -> 
                        queue:in({ From, Time, Msg }, Queue)
                end,
            loop(NewClock, NewQueue);
        stop ->
            printSafeMsgs(inf, Queue),
            ok
    end.
```

Tenemos dos funciones auxiliares que nos simplifican la situación. Cuando llega un mensaje,
como hemos dicho, actualizamos el tiempo para ese nodo, con el tiempo que nos llega a través del mensaje.
Luego debemos actualizar la cola de mensajes (usamos la interfaz de `queue` que trae Erlang).

Cuando es seguro imprimir ese mensaje, lo que hacemos es imprimirlo, e imprimir
los mensajes de la cola que también son seguros (todos aquellos en donde el tiempo de ese mensaje sea menor
que el tiempo recibido), y entonces actualizamos la cola devolviendo los mensajes que todavía
no son seguros de imprimir (los mensajes en los que todavía eso no se ha cumplido). Si el mensaje recibido
no es seguro de imprimir, lo que hacemos es encolarlo, aguardando a que llegue otro mensaje.

Notamos si en algún momento llega el mensaje de `stop`, es importante imprimir todos los mensajes
que se encuentran actualmente en la cola. Para eso lo que hacemos es pasar un tiempo infinito.

3) ¿Qué es lo que el log final nos muestra? ¿Los eventos ocurrieron en el mismo orden en que son presentados en el log? 

Sin los cambios que hicimos, el log mostraría simplemente el orden en que le llega los mensajes al Logger, según
su cola de mensajes. Con nuestra implementación lo que hacemos es esperar para imprimir los mensajes en el orden 
en que sucedieron las acciones en los demás procesos. Los eventos ocurren con los tiempos de los workers y por eso
el logger debe llevar cuenta de esos tiempos para imprimir los mensajes en el orden correcto, pero de esta forma
podemos garantizar el orden de los mensajes.

### Longitud de la cola de retención

4) ¿Que tan larga será la cola de retención? 

Con respecto al tamaño de la cola de retención detectamos que mientras menor sea
el `jitter` más posibilidades hay de encolar un mensaje por no ser seguro de
imprimir. Para un `jitter` de medio segundo lo que encontramos fue una cola que
como máximo tenía 15 elementos.

## Implementación con relojes vectoriales

Si bien con relojes lógicos de Lamport se logra ordenar los mensajes, el orden
total que impone sobre los mensajes hace que haya mensajes que podrían ser impresos, al no verse
afectados entre sí, pero que la implementación con estos relojes no permite detectar.

Una mejora sobre esto es implementar relojes vectoriales, que imponen solamente
un orden parcial sobre los mensajes. Así, si un mensaje va a esperar a ser impreso
solamente cuando el mensaje de un nodo que lo afecta todavía no ha llegado al logger. Esto
refleja mejor las dependencias entre los distintos nodos, y cómo se afectan unos a otros.

La modificación sobre el módulo time es la siguiente. En lugar de guardar solamente un
número, guardaremos una lista de tuplas de números y nodos. Cada uno de esos números
será como uno de los timestamps de lamport, pero la información adicional sobre los nodos
nos permitirá ir llevando cuenta de las dependencias.

Cada vez que haya un cambio en un nodo, actualizará su número en esa lista. Y como siempre,
pasara su reloj vectorial a los nodos a los que les mande mensaje. El nodo que reciba ese mensaje
lo que hará es incrementar en uno su número y para los otros nodo de su lista se quedará con el maximo
de tiempo comparado con los nodos de la lista que recibe en ese mensaje. 

Tuvimos que cambiar una parte del programa, dado que nos convenía que el reloj vectorial empiece con todos los nodos en cuestión.
Lo que hicimos fue pasar todos los nodos a la función `zero`, a través del mensaje `peers`.

En resumen, el módulo time nos quedó de esta manera:
```
zero(Nodes) ->
    lists:map(fun(N) -> { N, 0 } end, Nodes).

inc(Name, T) ->
	{ Name, OldTime } = lists:keyfind(Name, 1, T),
	lists:keyreplace(Name, 1, T, { Name, OldTime+1 }).

merge(Ti, Tj) ->
	ZipFunction = fun({N, T1}, {N, T2}) -> { N, max(T1, T2) } end,
	lists:zipwith(ZipFunction, Ti, Tj).

leq(Ti, Tj) ->
	PredFunc = 
		fun({ N, T1 }) ->
			{_, T2} = lists:keyfind(N, 1, Tj),
			T1 =< T2
		end, 
	lists:all(PredFunc, Ti).
```

## Conclusiones

Hemos aprendido un nuevo mecanismo de sincronización de procesos en sistemas distribuidos. 

Esta fue la actividad más compleja de todas, dado que tuvimos que leer teoría para comprender lo que queríamos lograr. También nos obligó
a generar distintos casos de pruebas que nos permitieron ir solucionando distintos errores que se nos fueron presentando. 

Aprendimos también sobre estructuras de datos que posee erlang, como listas y colas. Si bien podríamos haber usado maps en lugar de listas en
algunas partes del programa, optamos por la comodidad de la listas, sobre todo para la parte de relojes vectoriales, dado que nos simplificó el
trabajo.