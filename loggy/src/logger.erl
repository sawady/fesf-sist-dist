-module(logger).
-export([start/1, stop/1]).

start(Nodes) ->
    spawn_link(fun() ->init(Nodes) end).

stop(Logger) ->
    Logger ! stop.

init(Nodes) ->
    Clock = time:clock(Nodes),
    Queue = queue:new(),
    loop(Clock, Queue).
    
loop(Clock, Queue) ->
    receive
        {log, From, Time, Msg} ->
            NewClock = time:update(From, Time, Clock),
            NewQueue = 
                case time:safe(Time, NewClock) of
                    true  -> 
                        log(From, Time, Msg),
                        printSafeMsgs(Time, Queue),
                        notSafeMsgs(Time, Queue);
                    false -> 
                        queue:in({ From, Time, Msg }, Queue)
                end,
            % error_logger:info_msg("Queue length is: ~p~n", [queue:len(NewQueue)]),
            loop(NewClock, NewQueue);
        stop ->
            printQueueMsgs(Queue),
            ok
    end.

printSafeMsgs(Time, Queue) ->
    FilterFunction = fun({ _, T, _ }) -> time:leq(T, Time) end,
    SafeMsgs = queue:filter(FilterFunction, Queue),
    printQueueMsgs(SafeMsgs).

printQueueMsgs(Queue) ->
    SortFunction = fun({_, T1, _}, {_, T2, _}) -> time:leq(T1, T2) end,
    SortedMsgs = lists:sort(SortFunction, queue:to_list(Queue)),
    lists:foreach(
        fun({ From, T, Msg }) -> 
            log(From, T, Msg) 
        end, 
        SortedMsgs).

notSafeMsgs(Time, Queue) ->
    queue:filter(fun({ _, T, _ }) -> not time:leq(T, Time) end, Queue).
    
log(From, Time, Msg) ->
    io:format("log: ~w ~w ~p~n", [Time, From, Msg]).
