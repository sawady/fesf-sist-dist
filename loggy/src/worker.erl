-module(worker).
-export([start/5, stop/1, peers/3]).

start(Name, Logger, Seed, Sleep, Jitter) ->
    spawn_link(fun() -> init(Name, Logger, Seed, Sleep, Jitter) end).
    
stop(Worker) ->
    Worker ! stop.

init(Name, Log, Seed, Sleep, Jitter) ->
    random:seed(Seed, Seed, Seed),
    receive
        {peers, Nodes, Peers} ->
            Time = time:zero(Nodes),
            loop(Name, Log, Peers, Sleep, Jitter, Time);
        stop ->
            ok
    end.
    
peers(Wrk, Nodes, Peers) ->
    Wrk ! {peers, Nodes, Peers}.

loop(Name, Log, Peers, Sleep, Jitter, OldTime) ->
    Wait = random:uniform(Sleep),
    receive
        {msg, Time, Msg} ->
            NewTime =  time:merge(Time, time:inc(Name, OldTime)),
            Log ! {log, Name, NewTime, {received, Msg}},
            loop(Name, Log, Peers, Sleep, Jitter,  NewTime);
        stop ->
            ok;
        Error ->
            Log ! {log, Name, time, {error, Error}}
    after Wait ->
        Time = OldTime,
        Selected = select(Peers),
        Message = {hello, random:uniform(100)},
        Selected ! {msg, Time, Message},
        jitter(Jitter),
        Log ! {log, Name, Time, {sending, Message}},
        loop(Name, Log, Peers, Sleep, Jitter, time:inc(Name, OldTime))
    end.
    
select(Peers) ->
    lists:nth(random:uniform(length(Peers)), Peers).

jitter(0) -> ok;
jitter(Jitter) -> timer:sleep(random:uniform(Jitter)).
