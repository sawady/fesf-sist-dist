-module(server).
-export([start/1, open/1]).

-import(handler, [start/3]).
-import(validator, [start/0]).

start(N) ->
    spawn(fun() -> init(N) end).

init(N) ->
    Store = store:new(N),
    Validator = validator:start(),
    server(Validator, Store).
    
open(Server) ->
    Client = self(),
    Server ! {open, Client},
    receive
        {transaction, Validator, Store} ->
            handler:start(Client, Validator, Store) % devuelve el PID del handler
    end.
    
server(Validator, Store) ->
    receive
        {open, Client} ->
            Client ! { transaction, Validator, Store },
            server(Validator, Store);
        stop ->
            store:stop(Store)
    end.