-module(test).
-export([startServer/1, startClient/2, startClients/2, benchmark/2]).

-import(server, [start/1]).
-import(client, [read/2, write/3, commit/2, abort/0]).

startServer(N) -> 
    register(server, server:start(N)).
    
startClient(COLLECTOR, M) ->
    {_, _, Start} = os:timestamp(),
    Handler = server:open(server),
    random:seed(now()),
    I1 = random:uniform(M),
    I2 = random:uniform(M),
    N1 = client:read(Handler, I1), % aparece un 0
    client:write(Handler, I1, "Mi abuela pateaba calefones... snif snif"),
    client:write(Handler, I2, "Una telaraña se columpaba sobre un elefante"),
    N2 = client:read(Handler, I1), % aparece un mensaje nuevo
    N3 = client:read(Handler, I2), % aparece un mensaje nuevo
    STATUS = client:commit(Handler),
    {_, _, End} = os:timestamp(),
    COLLECTOR ! { self(), STATUS, (End - Start) / 1000 }.
    % io:format("PID ~p STATUS ~p ~n", [self(), STATUS]),
    % io:format("PID ~p ends with ~p ms. ~n", [self(), (End - Start) / 1000]).
    

benchmark(M, N) ->
    startServer(M),
    startClients(M, N).
    
startClients(M, N) -> 
    COLLECTOR = self(),
    [ spawn(fun() -> startClient(COLLECTOR, M) end) || _ <- lists:seq(1, N)],
    RESULTS = [ collectResult() || _ <- lists:seq(1, N)],
    { avgRespTime(RESULTS), count(abort, RESULTS), count(ok, RESULTS) }.
    
collectResult() ->
    receive
        { PID, STATUS, RT } -> { PID, STATUS, RT } 
    end.

avgRespTime(RESULTS) ->
    lists:sum(lists:map(fun({ _, _, RT }) -> RT end, RESULTS)) / length(RESULTS).
    
count(RES, RESULTS) ->
    length([R || { _, R, _ } <- RESULTS, R == RES]).