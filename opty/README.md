# Opty

## Introducción

El trabajo de esta semana consistió en la implementación de un store distribuido
y transaccionado. Este store es similar a un array, dado que sus entradas
están indexadas con números. Cada entrada es un proceso independiente y existe
una cantidad fija de los mismos, definida en la creación del store.

A su vez existe un servidor a la espera de clientes que abren nuevas transacciones.

## Completitud del código

Completamos varias partes del código de este trabajo. En particular, se pedía
completar partes de el handler, el validador y el servidor.

Con respecto al handler la función principal resultó así:

```
handler(Client, Validator, Store, Reads, Writes) ->
    receive
        {read, Ref, N} ->
            case lists:keysearch(N, 1, Writes) of
                {value, {N, _, Value}} ->
                    Client ! {Ref, Value},
                    handler(Client, Validator, Store, Reads, Writes);
                false ->
                    Entry = store:lookup(N, Store),
                    Entry ! {read, Ref, self()},
                    handler(Client, Validator, Store, Reads, Writes)
            end;
        {Ref, Entry, Value, Time} ->
            Client ! {Ref, Value},
            Added = [{Entry, Time}|Reads],
            handler(Client, Validator, Store, Added, Writes);
        {write, N, Value} ->
            Entry = store:lookup(N, Store),
            Added = [{N, Entry, Value}|Writes],
            handler(Client, Validator, Store, Reads, Added);
        {commit, Ref} ->
            Validator ! {validate, Ref, Reads, Writes, Client};
        abort -> abort
    end.
```

El mensaje más difícil de completar creemos que fue `read`, dado que requiere
entender bién la lógica con la que se maneja el handler.

Del validador completamos lo siguiente:

```
check_reads(N, Tag) ->
    if
        N == 0 ->
            ok;
        true ->
            receive
                {Tag, ok} -> 
                    check_reads(N-1, Tag);
                {Tag, abort} -> 
                    abort
            end
    end.
    
update(Writes) ->
    lists:map(
        fun({_, Entry, Value}) ->
            Entry ! {write, Value}
        end,
        Writes),
    ok.
```

La primera función si bien fue entregada en la corrección del enunciado la habíamos
completado en clase. El validador igualmente nos resultó más simple que el handler.

Finalmente, del servidor completamos lo siguiente:

```
server(Validator, Store) ->
    receive
        {open, Client} ->
            Client ! { transaction, Validator, Store },
            server(Validator, Store);
        stop ->
            store:stop(Store)
    end.
```

Observamos que en esta parte la arquitectura termina de definirse, dado
que es donde se envían mensajes el cliente y el servidor, pero con la ejecución
del proceso de un cliente de una función en el módulo del servidor. Pensamos
en refactorizar el código llevando la función `open` hacia el módulo de los
clientes. No obstante, preferimos dejarlo así dado que dentro de `open` se realiza
una llamada explícita al handler, y creemos que es mejor que el acoplamiento entre
handler y servidor es mejor que tener un acoplamiento entre el cliente y el handler,
más allá de que de alguna forma están acoplados al saber el cliente la interfaz
de mensajes que recibe un handler.

## Análisis de la arquitectura

### Transacciones

En el diseño de esta arquitectura el cliente abre una transacción y recibe
el PID de un handler que va a manejar las operaciones sobre dicha transacción.
Puede escribir y leer el store, y commitear finalmente sus operaciones. En caso
de que otro proceso haya modificado alguna de las entradas que leyó un cliente,
antes de finalizar su commit, la transacción es abortada y las operaciones no son
aplicadas. El trabajo de chequear la validez de las operaciones a aplicar 
es realizado por un proceso validador, que es corrido junto al servidor.

Hay procesos linkeados entre sí, a drede, para que cuando muere el proceso
que instanció a otros termine con estos. Las entradas del store están linkeados
al proceso del store, el validador está linkeado al servidor,
el handler está linkeado al cliente.

Desde nuestro punto de vista eso última decisión es la más controversial,
aunque entendible, dado que no es deseable un handler vivo sin un cliente asociado.
Tal vez sea posible replantear la arquitectura haciendo que haya un pool de handlers
del lado del servidor.

Por otra parte, nos preguntamos cómo es que los clientes saben el tamaño del store.
Notamos que no existe ningún chequeo sobre el número de entradas que puede
acceder un cliente en el store. Se podrían agregar dos cosas:
1) Se informa al cliente que ha accedido un indice inválido
2) Se informa el tamaño del store al cliente.

Esa funcionalidad ayudaría a los clientes a formar programas más interesantes.

## Pruebas de carga

### Performance general

La performance del servidor varía. Cuando se conectan muchos clientes tarda
más en responderle a cada uno. El tamaño del store no parece influir en los
tiempos de respuesta. Para una prueba con 100 mil procesos demoraba aproximadamente
300 ms. en completar la prueba de cada uno.

### Limitaciones

¿Cuáles son las limitaciones en el número de transacciones concurrentes y la tasa de éxito?
La tasa de exito depende fuertemente de la relacion entre el numero de transacciones y el 
tamaño del Store. Para aumentar la tasa de exito, usamos escrituras en posiciones aleatorias del 
store, simulando asi un uso mas real del mismo. 

### Sobre el realismo de la implementación

La implementación no es del todo realista. El validador es un cuello de botella, 
dado que es un único proceso validando los commits de todos los usuarios. Más 
clientes hacen commit más tarda en responderle a cada uno.

El handler corre en el cliente, y se le pasan el PID del servidor y el validador. Si se desea
distribuir el trabajo deberían crearse varios servidores con sus respectivos validadores,
los cuales seguramente posean una copia del store para operar de forma más eficiente.

Uno de los pro es que el proceso del handler al linkearse con el cliente muere si el cliente
muere. Pero la mayor contra es que el servidor no tiene control sobre el handler, por lo que
es posible abrir un número descontrolado de handlers, o incluso que un mismo cliente abra
todos los handlers que desee.
