# Detección de Fallas

## Introducción

La tolerancia a fallos es una de las características más deseables en los sistemas distribuidos, ya que es usual que estos sistemas intenten garantizar la ejecución correcta y continuada del software. El objetivo es conseguir que un sistema sea altamente fiable. Los fallos pueden ser permanentes o transitorios, y pueden provenir de componentes del hardware en donde ejecutan estos sistemas, de los sistemas de comunicación que utilicen o de errores de ejecución del mismo software (por fallos de programación o incluso por una especificación incorrecta).

De esta manera, los sistemas distribuidos implementan distintos mecanismos para hacer frente a los errores o fallos, planteando formas de recuperarse o simplemente actuar ante estas eventualidades.

## Detección de Fallos en Erlang

En general, en Erlang muchas veces el camino que se toma, para que el sistema sea tolerante a fallos, es el de permitir a la aplicación poder terminar y reiniciar procesos rápidamente. Los procesos emiten señales, y una forma de matar un proceso cuando otro termina por alguna razón, es a través de un mecanismo llamado `link`. Además, existen distintas formas de linkear procesos.

# Monitores

Nosotros analizaremos otro mecanismo, que utiliza el concepto de `monitors`. Los monitors poseen dos características: son unidireccionales y se pueden apilar. Los monitores se suelen utilizar para informar a un procesó que ocurrió con otro, pero en casos en donde uno no es totalmente dependiente del otro. Así, es común que en Erlang unos procesos estén monitoreando a otros, para informar a terceros sobre el estado de estos.

Cuando se está monitoreando a un proceso que se cae por alguna razón, se recibe el mensaje `{'DOWN', MonitorReference, process, Pid, Reason}`. Como los monitores son apilables, es posible recibir más de un mensaje de `'Down'`, pero el parámetro `MonitorReference` indicará de que monitor proviene el mensaje.

# Implementación

Se implemento el módulo *producer* con la interfaz `start/1, stop/0, crash/0`, y el módulo *consumer* con la interfaz `start/1, stop/0`.

La inicialización y el proceso recursivo de *consumer* quedaron de la siguiente manera:

```
init(Producer) ->
    Monitor = monitor(process, Producer),
    Producer ! {hello, self()},
    consumer(0, Monitor).

checkNumbers(N, M) -> 
    if
      N =:= M -> io:format("ping ~p~n", [M]);
      M > N -> io:format("el numero fue mas alto M:~p N:~p~n", [M, N]);
      true  -> io:format("caso_no_contemplado~n")
    end.
    
consumer(N, Monitor) ->
    receive
        { ping, M } ->
            checkNumbers(N, M),
            consumer(N+1, Monitor);
        {'DOWN', Monitor, process, Object, Info} ->
            io:format("~w died; ~w~n", [Object, Info]),
            consumer(N, Monitor);
        bye -> bye;
        stop -> stop
    end.
```

Se puede observar que se iniciar un proceso monitor para el *producer*, que avisará al *consumer* si se detecta que el primero está caído.

### Prueba en dos nodos

Utilizamos el software `Hamachi` para montar una LAN sobre la VPN que ofrece. Así levantamos nodos con las ips que nos ofrecía y logramos la comunicación en dos máquinas entre el *producer* y el *consumer*.

Las instrucciones para levantar la máquina *producer* fueron:
```
erl -name elias@IP1_HAMACHI -setcookie hamachi
$> producer:start(1000).
```

Las instrucciones para levantar la máquina *consumer* fueron:
```
erl -name sawady@IP2_HAMACHI -setcookie hamachi
$> consumer:start({ producer, 'elias@IP1_HAMACHI' }).
```

Luego de esto se recibierón normalmente mensajes de ping del *producer* para el *consumer*.

### Respuestas a preguntas del texto

1) **¿Qué mensaje se da como razón cuando el nodo es terminado? ¿Porqué?**

Se da el mensaje "noconnection", porque el monitor detecta que el proceso
ya no está corriendo.

2) **¿Qué sucede si matamos el nodo Erlang en el producer?**

Si matamos el nodo de erlang: `{producer,'elias@25.1.19.97'} died; noconnection`.

Si hacemos un crash sobre el producer: `{producer,'elias@25.1.19.97'} died; {badarith,[{producer, ...`.

Si el producer todavía no estaba levantado: ` {producer,'elias@25.1.19.97'} died; noproc ...`.

3) **Ahora probemos desconectar el cable de red de la máquina corriendo el producer y volvamos a enchufarlo despues de unos segundos. ¿Qué pasa?**

Retoma y sigue normalmente.

4) **Desconectemos el cable por períodos mas largos. ¿Qué pasa ahora?**

Pasó lo siguiente. Ibamos por el "ping 45", desconectamos un rato largo (más de 1 min), el monitor no avisaba nada hasta el momento. Volvimos a enchufar y dijo `{producer,'elias@25.1.19.97'} died; noconnection`. Pero inmediatamente después empezo a resolver ping de nuevo, sòlo que estaba desfasado, porque el consumer imprimía lo siguiente: `el numero fue mas alto M:67 N:47`. Y siguió así, siempre desfasado por 20.

5) **¿Qué significa haber recibido un mensaje 'DOWN'? ¿Cuándo debemos confiar en el?**

Significa que el monitor detectó que el proceso monitoreado está caído por alguna razón, lo que no significa que pueda llegar a lograrse la comunicación nuevamente. Creemos que el mensaje del monitor es sólo informativo, y en algún momento se debe tomar la decisión de dar por muerta la conexión con el otro nodo, con un timeout por ejemplo.

6) **¿Se recibieron mensajes fuera de orden, aun sin haber recibido un mensaje 'DOWN'?**

Se recibió un 'DOWN' en un momento, y luego se empezaron a recibir mensajes fuera de orden.

7) **¿Qué dice el manual acerca de las garantías de envíos de mensajes?**

Se puede asumir que los mensajes se encolan en el orden en que llegaron a su destino, pero eso no significa que mensajes que se hayan enviado de una máquina a otra lleguen efectivamente a destino. En otras palabras, es posible que se pierdan mensajes en la comunicación.
