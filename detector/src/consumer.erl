-module(consumer).
-export([start/1, stop/0]).

start(Producer) ->
    Consumer = spawn(fun() -> init(Producer) end),
    register(consumer, Consumer).

stop() ->
    consumer ! stop.

init(Producer) ->
    Monitor = monitor(process, Producer),
    Producer ! {hello, self()},
    consumer(0, Monitor).

checkNumbers(N, M) -> 
    if
      N =:= M -> io:format("ping ~p~n", [M]);
      M > N -> io:format("el numero fue mas alto M:~p N:~p~n", [M, N]);
      true  -> io:format("caso_no_contemplado~n")
    end.
    
consumer(N, Monitor) ->
    receive
        { ping, M } ->
            checkNumbers(N, M),
            consumer(N+1, Monitor);
        {'DOWN', Monitor, process, Object, Info} ->
            io:format("~w died; ~w~n", [Object, Info]),
            consumer(N, Monitor);
        bye -> bye;
        stop -> stop
    end.
    