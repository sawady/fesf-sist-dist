-module(lock3).
-export([start/1]).

start(Id) ->
    spawn(fun() -> init(Id) end).

init(_) ->
    receive
        { peers, Peers } ->
        Time = time:zero(),
        open(Time, Peers);
    stop ->
        ok
    end.

open(Time, Nodes) ->
    receive
        {take, Master} ->
            NewTime = Time+1,
            Refs = requests(NewTime, Nodes),
            Waiting = [],
            wait(NewTime, Nodes, Master, Refs, Waiting);
        {request, OtherTime, From, Ref} ->
            From ! {ok, Ref},
            open(time:merge(Time, OtherTime), Nodes);
        stop ->
            ok
    end.
    
requests(Time, Nodes) ->
    lists:map(fun(P) -> 
        R = make_ref(),
        P ! {request, Time, self(), R}, 
        R 
    end, Nodes).
    
wait(Time, Nodes, Master, [], Waiting) ->
    Master ! taken,
    held(Time, Nodes, Waiting);

wait(Time, Nodes, Master, Refs, Waiting) ->
    receive
        {request, OtherTime, From, Ref} ->
            NewTime = time:merge(Time, OtherTime),
            case time:leq(Time, OtherTime) of
                true  -> 
                    wait(NewTime, Nodes, Master, Refs, [ {From, Ref} | Waiting ]);
                false -> 
                    From ! {ok, Ref},
                    wait(NewTime, Nodes, Master, Refs, Waiting)
            end;
        {ok, Ref} ->
            Refs2 = lists:delete(Ref, Refs),
            wait(Time, Nodes, Master, Refs2, Waiting);
        release ->
            ok(Waiting),
            open(Time, Nodes)
    end.

ok(Waiting) ->
    lists:foreach(fun({F,R}) -> 
        F ! {ok, R} 
    end, Waiting).
    
held(Time, Nodes, Waiting) ->
    receive
        {request, OtherTime, From, Ref} ->
            held(time:merge(Time, OtherTime), Nodes, [ {From, Ref} | Waiting ]);
        release ->
            ok(Waiting),
            open(Time, Nodes)
    end.