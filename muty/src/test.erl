-module(test).
-export([test1/1,test2/1,test3/1]).

test1(Size) ->
	testX(lock1, "Lock3 ", Size).

test2(Size) ->
	testX(lock2, "Lock3 ", Size).

test3(Size) ->
	testX(lock3, "Lock3 ", Size).

testX(LockModule, LockName, Size) ->
	% generate data
	Data = lists:map(fun(N) ->
		Lock  = LockModule:start(LockName ++ integer_to_list(N)),
		Name  = "Worker " ++ integer_to_list(N),
		Seed  = N,
		Sleep = 200,
		Work  = 3000,		
		{ Name, Lock, Seed, Sleep, Work }
	end, lists:seq(1, Size)),

	% get all locks
	Locks = lists:map(fun({ _, Lock, _, _, _ }) -> 
		Lock
	end, Data),

	% open locks
	lists:foreach(fun(Lock) -> 
		Lock ! { peers, lists:delete(Lock, Locks) }
	end, Locks),

	% wake up workers
	lists:foreach(fun({ Name, Lock, Seed, Sleep, Work }) -> 
		worker:start(Name, Lock, Seed, Sleep, Work)
	end, Data).



	% 