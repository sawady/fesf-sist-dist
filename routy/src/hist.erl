-module(hist).
-export([new/1, update/3]).

% Return a new history, where messages from Name will
% always be seen as old
new(Name) -> [ { Name, -1 } ].

% Check if message number N from the Node
% is old or new. If it is old then return old but 
% if it new return {new, Updated}
% where Updated is the updated history
update(Node, N, History) -> 
  case lists:keyfind(Node, 1, History) of
  	false -> { new, [ { Node, N } | History ] };
  	{ _, N2 }  ->
	  case N2 < N of
	  	true  -> { new, lists:keyreplace(Node, 1, History, { Node, N }) };
	  	false -> old
	  end
  end.