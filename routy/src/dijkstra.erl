-module (dijkstra).
-export ([table/2, route/2]).

% returns the length of the shortest path to the
% node or 0 if the node is not found.
entry(Node, Sorted) ->
	case lists:keyfind(Node, 1, Sorted) of
		false -> 0;
		{ _, N, _ } -> N
	end.

% replaces the entry for Node
% in Sorted with a new entry having a new length N and Gateway.
% The resulting list should of course be sorted.
replace(Node, N, Gateway, Sorted) ->
	lists:keysort(2, lists:keyreplace(Node, 1, Sorted, { Node, N, Gateway })).

% update the list Sorted given
% the information that Node can be reached in N hops using Gateway.
% If no entry is found then no new entry is added. Only if we have a
% better (shorter) path should we replace the existing entry
update(Node, N, Gateway, Sorted) ->
    case entry(Node, Sorted) of
    	0 -> Sorted;
    	N2 when N >= N2 -> Sorted;
    	N2 when N <  N2 -> replace(Node, N, Gateway, Sorted)
    end.

% construct a table given a sorted list
% of nodes, a map and a table constructed so far
iterate(Sorted, Map, Table) ->
	case Sorted of
		[] -> Table;
		[ { _, inf, unknown } | _ ] -> Table;
		[ { Node, N, Gateway } | SL ] -> 
			RCH   = map:reachable(Node, Map),
			NewSL = lists:foldl(fun(X, R) -> update(X, N+1, Gateway, R) end, SL, RCH),
			iterate(NewSL, Map, lists:keystore(Node, 1, Table, { Node, Gateway }))
	end.

% construct a routing table given the gateways
% and a map
table(Gateways, Map) ->
	SortedMapNodes    = [ { X, inf, unknown } || X <- map:all_nodes(Map) ],
	List = lists:foldl(
		fun({ X, 0, X }, R) -> 
			lists:keystore(X, 1, R, { X, 0, X }) 
		end, 
		SortedMapNodes, 
		[ { X, 0, X } || X <- Gateways ]
	),
	SortedList = lists:keysort(2, List),
	iterate(SortedList, Map, []).


route(Node, Table) ->
	case lists:keyfind(Node, 1, Table) of
		false -> notfound;
		{ _, Gateway} -> {ok, Gateway}
	end.