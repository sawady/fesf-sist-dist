-module(rudy).
-export([start/1, stop/0]).
-import(http, [parse_request/1]).
-import(counter, [initialize/0]).

start(Port) ->
    register(counter, counter:initialize()),
    R = spawn(fun() -> init(Port) end),
    register(rudy, R).

stop() ->
    exit(whereis(rudy), "time to die").

init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} ->
            handler(Listen),
            gen_tcp:close(Listen),
            ok;
        {error, _} ->
            error
    end.

handler(Listen) ->
    A = gen_tcp:accept(Listen),
    spawn(
        fun() ->
            case A of
                {ok, Client} ->
                    request(Client),
                    ok;
                {error, _} ->
                    error
            end
        end
    ),
    handler(Listen).
    
request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
    case Recv of
        {ok, Str} ->
            Request  = parse_request(Str),
            Response = reply(Request),
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("rudy: error: ~w~n", [Error])
    end,
    gen_tcp:close(Client).

reply({{get, URI, _}, _, _}) ->
    timer:sleep(40),
    error_logger:info_msg("Routing: " ++ URI),
    case URI of
        "/" -> counter();
        "/loaderio-3f3379c7d83844d9b437c031a047d9b8/" -> http:ok("loaderio-3f3379c7d83844d9b437c031a047d9b8");
        _Else -> http:ok("No Resource")
    end.

counter() ->       
    N = getNumber(),
    http:ok(io_lib:format("~p", [N]) ++ " elefantes se columpeaban sobre la tela de una araña").
    
getNumber() ->
    counter ! { next, self() },
    receive
        { counter, N } -> N
    end.