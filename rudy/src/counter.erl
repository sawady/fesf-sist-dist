-module(counter).
-export([initialize/0]).

initialize() ->
    spawn(fun() -> counter(0) end).

counter(N) -> 
    receive
        { next, PID } ->
            PID ! { counter, N },
            counter(N+1);
        stop -> 
            stop
    end.