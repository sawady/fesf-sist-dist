Rudy
=====

## Introducción

El trabajo de esta semana consistió en la implementación de un webserver
sencillo, que sólo recibe requests simplificados de tipo GET. 

## Desarrollo

Comenzamos implementando el servidor tal cual lo indicaba el ejercicio. No
tuvimos complicaciones al respecto. Probamos hacer peticiones desde una consola
en otra pc y desde el navegador.

Luego, para volverlo más interesante, le agregamos un proceso que hacía 
de contador, que nos permitía tener "estado" entre requests. Así,
se servía ante cualquier URI una página que contaba cuántas peticiones
le habían llegado desde que inició el server.

En ese punto nos dimos cuenta de algo: el navegador hacía dos peticiones y no una.
Nos llevó unos minutos descubrir que el navegador pedía el favicon de la página.
Lo que hicimos fue un `case` donde explicitamos las URI que soportamos. Ahora si
nos llegaba la URI `/counter`, ejecutábamos la función que mandaba un mensaje
al contador. En cualquier otro caso seguimos devolviendo un status 200 pero
con un string cualquiera. Una mejora en este punto consistiría en devolver un
status 404, indicando que el recurso pedido no fue encontrado.

## Benchmark

Corrimos los benchmarks solicitados. El servidor se encuentra corriendo en
este [sitio de cloud9](https://erlang-sawady.c9users.io/), y los tests los hicimos 
tanto corriendo el benchmark que trae el ejercicio como con 
[loader.io](https://loader.io).

Integrar la herramienta loader.io fue sumamente fácil. Simplemente
agregamos un PATH más devolviendo una key que nos daba el sitio.
Tampoco hubo problemas para correr el servidor en cloud9. El único inconveniente
es que si no recibe requests en un tiempo considerable, la VM se duerme, y por
ende mata al servidor.

Las preguntas que respondimos son las siguientes:

### a) ¿cuántos requests por segundo podemos servir?

#### Sin Delay

Primero lo probamos sin delay, con y sin logging a consola.

Hicimos el siguiente test: *arrancar con 250 clientes simultáneos durante 1 minuto*.

El tiempo promedio de respuesta fue de 74ms, lo que quiere decir que por
segundo servíamos en promedio 0.0074 requests. En un minuto llegó a servir
15 mil requests. Hubo un pico de aproximadamente 1 seg en unos request, en los
que creemos que la máquina virtual hizo garbage collection, pero no estamos
seguros.

Los resultados del test se pueden encontrar en el siguiente [link](http://bit.ly/20DmuFa).

#### Con Delay

Luego agregamos el delay pedido, sacando el logging a consola.

El test fue el mismo: *250 clientes simultáneos durante 1 minuto".

Los resultados fueron otros: tiempo promedio de respuesta de 1915ms, lo que quiere
decir que servíamos 1.9 requests por segundo. Sólo se sirvieron 117 requests
en ese minuto. Es decir, se cubrió sólo un 0,78% de los 15 mil requests del test anterior.

Los resultados del test se pueden encontrar en el siguiente [link](http://bit.ly/1qJ0agU).

#### Con la mejora de un spawn por request

Luego implementamos la mejora de un proceso por request. Básicamente en este
caso el servidor no esperaba para volver a escuchar por otro request. Realizamos
nuevamente los tests anteriormente mencionados.

Primero lo probamos sin delay, y los resultados fueron más o menos los mismos,
con alguna ligera variación en el tiempo promedio de respuesta, pero se mantenía
en los 75ms.

La prueba definitiva consistió en agregar un delay de 1 seg entre requests. El
tiempo promedio de respuesta fue de 1 seg durante la mayor parte del minuto de
prueba, pero luego empezó a oscilar bruscamente y terminó en 1.9 seg por respuesta,
lo cual es mejor que la implementación sin múltiples procesos atendiendo los
requests, pero igualmente peor que la versión sin delay. Es probable que la
cantidad de procesos corriendo haya empeorado la eficiencia del sistema en general.

Este último test puede encontrarse en el siguiente [link](http://bit.ly/1NwezBO).

### b) ¿Nuestro delay artificial es importante o desaparece dentro del overhead de parsing? 

Parece importar dado que los tiempos de respuesta empeoran enórmemente en presencia del delay,
ya sea en el caso con un sólo proceso o el caso con múltithreading.

### c) ¿Qué ocurre si ejecutamos los benchmarks en varias máquinas al mismo tiempo? 

En el caso sin delay, los tiempos de respuesta se mantienen. En el caso de tener el delay,
los tiempos de respuesta empeoran aún más.
