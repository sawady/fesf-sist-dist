-module(test).
-export([run/3]).

run(Size, Jitter, Sleep) ->
	% generate data
	Data = lists:map(fun(N) ->
		Multicaster  = multicaster:start("MultiCaster " ++ integer_to_list(N), Jitter),
		Name  = "Worker " ++ integer_to_list(N),
		Seed  = N,	
		{ Name, Multicaster, Seed }
	end, lists:seq(1, Size)),

	% get all locks
	Multicasters = lists:map(fun({ _, Multicaster, _ }) -> 
		Multicaster
	end, Data),

	% open locks
	lists:foreach(fun(Multicaster) -> 
		Multicaster ! { peers, lists:delete(Multicaster, Multicasters) }
	end, Multicasters),

	% wake up workers
	lists:foreach(fun({ Name, Multicaster, Seed }) -> 
		worker:start(Name, Multicaster, Seed, Sleep)
	end, Data).



	% 