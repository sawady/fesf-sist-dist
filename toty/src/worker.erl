-module(worker).
-export([start/4]).

-define(deadlock, 4000).

start(Name, MultiCast, Seed, Sleep) ->
    spawn(fun() -> init(Name, MultiCast, Seed, Sleep) end).
    
init(Name, MultiCast, Seed, Sleep) ->
    MultiCast ! { master, self() },
    Gui = spawn(gui, init, [Name]),
    random:seed(Seed, Seed, Seed),
    State = { 0, 0, 0 },
    worker(MultiCast, State, Sleep, Gui),
    Gui ! stop.

worker(MultiCast, State, Sleep, Gui) ->
    Wait = random:uniform(Sleep),
    receive
        stop ->
            stop
    after Wait ->
        N = random:uniform(20),
        MultiCast ! { send, N },
        NewState = paint(N, State, Gui),
        worker(MultiCast, NewState, Sleep, Gui)
    end.

paint(N, State, Gui) ->
    receive
        { response, N } -> 
            NewState = addN(State, N),
            Gui ! { paint, NewState },
            NewState;
        { response, N2 } -> 
            NewState = addN(State, N2),
            Gui ! { paint, NewState },
            paint(N, NewState, Gui)
    end.

addN({ R, G, B }, N) -> { G, B, (R+N) rem 256 }.
